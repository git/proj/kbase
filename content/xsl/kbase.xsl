<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="issue">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><xsl:value-of select="title"/></title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <link href="styles.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
  <!--
  -->
  </style>
</head>
<body>

<h1><xsl:value-of select="title"/></h1>

<xsl:apply-templates />
</body>
</html>

</xsl:template>

<xsl:template match="synopsis">

<h2>Synopsis</h2>

<xsl:apply-templates />

</xsl:template>

<xsl:template match="environment">

<h2>Environment</h2>

<xsl:apply-templates />

</xsl:template>

<xsl:template match="analysis">

<h2>Analysis</h2>

<xsl:apply-templates />

</xsl:template>

<xsl:template match="solution">

<h2>Solution</h2>

<xsl:apply-templates />

</xsl:template>

<xsl:template match="p">
<p>
<xsl:apply-templates />
</p>
</xsl:template>

<xsl:template match="e">
<em><xsl:apply-templates/></em>
</xsl:template>

<xsl:template match="uri">
<a href="{@link}"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="pre">
<pre>
<xsl:apply-templates/>
</pre>
</xsl:template>

<xsl:template match="maintainers">
<p>
<b>Maintainers:</b> <xsl:apply-templates/>
</p>
</xsl:template>

<xsl:template match="keywords">
<p>
<b>Keywords:</b> <xsl:apply-templates/>
</p>
</xsl:template>

<xsl:template match="title"/>

</xsl:stylesheet>

